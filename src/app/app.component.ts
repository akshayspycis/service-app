import { Component, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { TranslateService } from '@ngx-translate/core';
import { Config, Nav, Platform } from 'ionic-angular';

import { FirstRunPage,MainPage } from '../pages/pages';
import { Settings, User } from '../providers/providers';
import {url} from '../pages/pages'
@Component({
  template: `<ion-menu [content]="content" *ngIf="getCheck()">
    <ion-header>
      <ion-toolbar>
        <ion-title>Profile</ion-title>
      </ion-toolbar>
    </ion-header>
    
    <ion-content>
    <div class="profile-image-wrapper" (click)="openPage({component:'ProfilepicPage'})">
      <img class="profile-image" [src] ="getPic()" />
      <ion-card-title>
        {{getUserName()}}
      </ion-card-title>
    </div>
      <ion-list>
        <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">
          {{p.title}}
        </button>
      </ion-list>
    </ion-content>

  </ion-menu>
  <ion-nav #content [root]="rootPage"></ion-nav>`
})
export class MyApp {
    rootPage = this.user.getUser()==null?FirstRunPage:MainPage;

  @ViewChild(Nav) nav: Nav;

  pages: any[] = [
    { title: 'Pay Pal', component: 'PaymentOptionsPage' },
    { title: 'Home', component: 'TabsPage' },
    { title: 'My Order', component: 'MyorderPage' },
    { title: 'Account Details', component: 'AccountDetailsPage' },
    { title: 'Address', component: 'SelectAddressPage' },
    { title: 'Profile Picture', component: 'ProfilepicPage' },
    { title: 'Password', component: 'PasswordPage' },
    { title: 'Logout', component: 'WelcomePage' },
    
//    { title: 'Content', component: 'ContentPage' },
//    { title: 'Login', component: 'LoginPage' },
//    { title: 'Signup', component: 'SignupPage' },
//    { title: 'Map', component: 'MapPage' },
//    { title: 'Master Detail', component: 'ListMasterPage' },
//    { title: 'Menu', component: 'MenuPage' },
//    { title: 'Settings', component: 'SettingsPage' },
//    { title: 'Search', component: 'SearchPage' }
  ]

  constructor(private translate: TranslateService, private platform: Platform, settings: Settings, private config: Config, private statusBar: StatusBar, private splashScreen: SplashScreen,public user:User) {
    this.initTranslate();
  }
  getCheck(){
      return this.user.getUser()==null?false:true;
  }
  getPic(){
      return this.user.getUser()==null || this.user.getUser().pic==null?'assets/img/header-profile-default.png':url+ this.user.getUser().pic.substring(3, this.user.getUser().pic.length);
  }
  getUserName(){
      return this.user.getUser() == null ? 'Error in display username' : this.capitalizeFirstLetter(this.user.getUser().user_name);
  }
  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  showPicture(){
      
  }
  ionViewDidLoad() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');

    if (this.translate.getBrowserLang() !== undefined) {
      this.translate.use(this.translate.getBrowserLang());
    } else {
      this.translate.use('en'); // Set your language here
    }

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
