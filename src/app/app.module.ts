import { ErrorHandler, NgModule } from '@angular/core';
import { Http, HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { Camera } from '@ionic-native/camera';
import { GoogleMaps } from '@ionic-native/google-maps';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule, Storage } from '@ionic/storage';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';

import { Items } from '../mocks/providers/items';
import { Settings } from '../providers/providers';
import { User } from '../providers/providers';
import { Api } from '../providers/providers';
import { MyApp } from './app.component';
import { ProductDetailsProvider } from '../providers/product-details/product-details';
import { ProblemDetailsProvider } from '../providers/problem-details/problem-details';
import { ProductCartProvider } from '../providers/product-cart/product-cart';
import { OrderProductProvider } from '../providers/order-product/order-product';
import {PopoverComponent} from '../components/popover/popover';
import { OtpProvider } from '../providers/otp/otp';
import { AddressDetailsProvider } from '../providers/address-details/address-details';
import { OrderProvider } from '../providers/order/order';
import { PaytmPaymentGatewayProvider } from '../providers/paytm-payment-gateway/paytm-payment-gateway';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';

// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function provideSettings(storage: Storage) {
  /**
   * The Settings provider takes a set of default settings for your app.
   *
   * You can add new settings options at any time. Once the settings are saved,
   * these values will not overwrite the saved values (this can be done manually if desired).
   */
  return new Settings(storage, {
    option1: true,
    option2: 'Ionitron J. Framework',
    option3: '3',
    option4: 'Hello'
  });
}

@NgModule({
  declarations: [
    MyApp,
    PopoverComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [Http]
      }
    }),
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PopoverComponent,
  ],
  providers: [
    Api,
    Items,
    User,
    File,
    Transfer,
    Camera,
    FilePath,
    GoogleMaps,
    SplashScreen,
    StatusBar,
    { provide: Settings, useFactory: provideSettings, deps: [Storage] },
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ProductDetailsProvider,
    ProblemDetailsProvider,
    ProductCartProvider,
    OrderProductProvider,
    OtpProvider,
    AddressDetailsProvider,
    OrderProvider,
    PaytmPaymentGatewayProvider,
    PayPal 
  ]
})
export class AppModule { }
