import { Component } from '@angular/core';
import {ViewController, NavParams} from "ionic-angular";
import {OrderProductProvider} from "../../providers/order-product/order-product";

/**
 * Generated class for the PopoverComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'popover',
  templateUrl: 'popover.html'
})
export class PopoverComponent {
  item: any;
  text: string;
  order_products: any=[];
   constructor(public viewCtrl: ViewController,private params: NavParams,public orderProduct:OrderProductProvider) {
    console.log('Hello PopoverComponent Component');
    this.item = this.params.get('getItem')();
    this.getProduct();
   }
   getProduct(){
      this.order_products = this.orderProduct.selectOrderProduct(this.item.product_details_id);
   }
   close() {
     this.viewCtrl.dismiss();
   }
   delProduct(order_product: any){
       this.orderProduct.deleteOrderProduct(order_product);
       this.getProduct();
   }
   
   
}
