import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import {User} from "../../providers/providers";
import {TranslateService} from "@ngx-translate/core";

@IonicPage()
@Component({
  selector: 'page-account-details',
  templateUrl: 'account-details.html',
})

export class AccountDetailsPage {
  contactfilter =  /^\d{10}$/;
  loader: any;
  account: {user_id: string,user_name: string,email: string, contact_no:string} = {
    user_id: '',
    user_name: '',
    email: '',
    contact_no: '',
  };
  emailfilter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  check_email=true;    
  // Our translated text strings
  private signupErrorString: string;
  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public translateService: TranslateService) {
    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
    if (this.user.getUser() == undefined){
       this.navCtrl.push("WelcomePage");
       return ;
    }
    this.account.user_name=this.user.getUser().user_name;
    this.account.email=this.user.getUser().email;
    this.account.contact_no=this.user.getUser().contact_no;
  }
  
  checkContactNo(){
    // Attempt to login in through our User service
      if (this.account.contact_no != this.user.getUser().contact_no){
          if (this.account.user_name==""){
            this.showError("Please Provide Valid Name.");
          } else if (this.account.contact_no == "" ||  !this.contactfilter.test(this.account.contact_no)){
              this.showError("Please Provide Valid Contact Number.");
          }else{
            this.presentLoading(true);
          }
      }else{
          this.presentLoading(false);
      }
      
  }
  getOtp(){
      this.user.getOtp(this.account).map(res => res.json()).subscribe((res) => {
          this.closeLoading();
            if(!res){
                this.showError("This Contact no is already exits.");
            }else{
                this.navCtrl.push('CheckotpPage', {'account': this.account,'module':this});
            }
        }, (err) => {
            this.closeLoading();
            this.showError("Internal Server Error");
        });
  }
  updAcoountDetails(){
      if(this.check_email){
      this.account.user_id = this.user.getUser().user_id;
      this.user.updAcoountDetails(this.account).map(res => res.json()).subscribe((res) => {
          this.closeLoading();
            if(!res){
                this.showError("Internal Server Error.");
            }else{
                this.showError("Account Details update sucessfully.");
                this.user.setAccounDetails(this.account);
            }
        }, (err) => {
            this.closeLoading();
            alert(err);
        });
      }else{
          this.closeLoading();
          this.showError("This Email Address already exits.");
      }
  }
  checkEmail(){
      if (this.account.email != this.user.getUser().email){
          if (this.emailfilter.test(this.account.email)){
            this.user.checkEmail(this.account).map(res => res.json()).subscribe((res) => {
                this.check_email=res;
                if(!this.check_email){
                    this.showError("This Email Address already exits.");
                }
              }, err => {
                  this.closeLoading();
                  alert("error"+err);
              });
          }
      }else{
          this.check_email=true;
      }
  }
  showError(message){
      let toast = this.toastCtrl.create({
        message: message,
        duration: 3000,
        position: 'top'
      });
      toast.present();
  }
   presentLoading(b) {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    this.loader=loader; 
        if(b){
            this.getOtp();
        }else{
            this.updAcoountDetails();
        }
    
  }
  closeLoading(){
      this.loader.dismiss();
  }
  
}
