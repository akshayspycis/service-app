import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {User} from "../../providers/providers";
import {url} from '../pages';
/**
 * Generated class for the AccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public user:User) {
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountPage');
  }
  getPic(){
      return this.user.getUser()==null || this.user.getUser().pic==null?'assets/img/header-profile-default.png':url+ this.user.getUser().pic.substring(3, this.user.getUser().pic.length);
  }
  getUserName(){
      return this.user.getUser() == null ? 'Error in display username' : this.capitalizeFirstLetter(this.user.getUser().user_name);
  }
  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  openPage(page) {
      this.navCtrl.push(page.component);
  }
  
}
