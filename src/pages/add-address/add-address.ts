import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import {AddressDetailsProvider} from "../../providers/address-details/address-details";
import {TranslateService} from "@ngx-translate/core";

/**
 * Generated class for the AddAddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-address',
  templateUrl: 'add-address.html',
})
export class AddAddressPage {
    
private signupErrorString: string;
obj: any;
state: string;
stateAlertOpts: { title: string, subTitle: string };
loader: any;
pincodefilter =  /^\d{6}$/;
account: {address_details_id:string,address_1: string, address_2:string,city:string,pincode:string,state:string} = {
    address_details_id: '',
    address_1: '',
    address_2: '',
    city: '',
    pincode: '',
    state: '',
};
constructor(public navCtrl: NavController, public navParams: NavParams,public translateService: TranslateService,public address_details_provider:AddressDetailsProvider,public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,) {
    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
    if(this.navParams.get("account")!=null) this.account= this.navParams.get("account");
    if(this.navParams.get("obj")!=null) this.obj= this.navParams.get("obj");
    this.stateAlertOpts = {
      title: 'State',
      subTitle: 'Select your State'
    };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddAddressPage');
  }
  
 showError(message){
      let toast = this.toastCtrl.create({
        message: message,
        duration: 3000,
        position: 'top'
      });
      toast.present();
  }
   presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    this.loader=loader; 
    this.insAddress();
  }
  closeLoading(){
      this.loader.dismiss();
  }
  submitAddress(){
     this.presentLoading();
  }
  insAddress(){
      var ok=this.checkValidation();
      if(ok=='ok'){
          this.address_details_provider.insAddress(this.account).map(res => res.json()).subscribe((res) => {
          this.closeLoading();
            if(!res){
                this.showError("Internal Server Error");
            }else{
                this.address_details_provider.loadForce();
                
                this.navCtrl.pop();
                this.obj.loadForce();
                
            }
        }, (err) => {
            this.closeLoading();
            this.showError(err);
        });
      }else{
          this.closeLoading();
          this.showError('Please Provide Valid '+ok+' .')
      }
      
  }
  checkValidation(){
      if (this.account.address_1==''){
          return 'Owner name / House No.';
      }else if (this.account.address_2==''){
          return 'Landmark.';
      }else if (this.account.city==''){
          return 'City.';
      } else if (this.account.pincode == '' || !this.pincodefilter.test(this.account.pincode)){
          return 'Pincode.';
      }else if (this.account.state==''){
          return 'State.';
      }else {
          return 'ok';
      }
  }
}
