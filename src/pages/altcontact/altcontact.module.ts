import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { AltcontactPage } from './altcontact';

@NgModule({
  declarations: [
    AltcontactPage,
  ],
  imports: [
    IonicPageModule.forChild(AltcontactPage),
    TranslateModule.forChild()
  ],
})
export class AltcontactPageModule {}
