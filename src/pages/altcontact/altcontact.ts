import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import {TranslateService} from "@ngx-translate/core";
import {OrderProductProvider} from "../../providers/order-product/order-product";
import {OrderProvider} from "../../providers/order/order";

/**
 * Generated class for the AltcontactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-altcontact',
  templateUrl: 'altcontact.html',
})
export class AltcontactPage {
contactfilter =  /^\d{10}$/;
private signupErrorString: string;
account: {alt_contact_no: string, address_details_id: string,order_products: {}} = {
    alt_contact_no: '',
    address_details_id: '',
    order_products: null
};
loader: any;
no_of_item: number=0;
total: number=0;
total_discount: number=0;
total_estimate: number=0;
order_products: any;

constructor(public navCtrl: NavController, public navParams: NavParams,public translateService: TranslateService,public toastCtrl: ToastController,public orderProduct:OrderProductProvider,public order_provider:OrderProvider,
    public loadingCtrl: LoadingController) {
    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
    if(this.navParams.get("address_details_id")!=null) this.account.address_details_id= this.navParams.get("address_details_id");
    this.getProduct();
  }
  getProduct(){
    this.no_of_item=0;
    this.total=0;
    this.total_discount=0;
    this.total_estimate=0;
    this.order_products = this.orderProduct.selectAllOrderProduct();
//    this.account.order_products = JSON.stringify(this.order_products).replace("[", "").replace("]","");
    this.account.order_products = this.order_products;
    this.no_of_item = this.order_products.length;
    for (let value in this.order_products) {
        this.total= this.total + parseInt(this.order_products[value].service_charge);
     }
    this.total_estimate = this.total;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AltcontactPage');
  }
   showError(message){
      let toast = this.toastCtrl.create({
        message: message,
        duration: 3000,
        position: 'top'
      });
      toast.present();
  }
  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    this.loader=loader; 
    this.insOrder();
  }
  closeLoading(){
      this.loader.dismiss();
  }
  submitAddress(){
     this.presentLoading();
  }
  insOrder(){
      var ok=this.checkValidation();
      if(ok=='ok'){
          this.order_provider.insOrder(this.account).map(res => res.json()).subscribe((res) => {
          this.closeLoading();
            if(!res){
                this.showError("Internal Server Error");
            }else{
                this.orderProduct.erroseAllOrderProduct();
                this.navCtrl.setRoot('OrderdonePage',{'order_no':res});
            }
        }, (err) => {
            this.closeLoading();
            this.showError(err);
        });
      }else{
          this.closeLoading();
          this.showError('Please Provide Valid '+ok+' .')
      }
  }
  checkValidation(){
      if (this.account.alt_contact_no != '' && this.contactfilter.test(this.account.alt_contact_no)){
          return 'ok';
      }else {
          return 'Alternative Contact no.';
      }
  }
  continue(){
      this.presentLoading();
  }
}
