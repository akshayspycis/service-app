import { NgModule } from '@angular/core';
import { TranslateModule } from "@ngx-translate/core";
import { IonicPageModule } from 'ionic-angular';

import { CheckotpPage } from './checkotp';

@NgModule({
  declarations: [
    CheckotpPage,
  ],
  imports: [
    IonicPageModule.forChild(CheckotpPage),
    TranslateModule.forChild()
  ],
  exports: [
    CheckotpPage
  ]
})
export class CheckotpPageModule {}
