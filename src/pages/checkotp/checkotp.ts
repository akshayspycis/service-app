import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, Nav, ToastController, LoadingController  } from 'ionic-angular';
import {User} from "../../providers/providers";
declare var cordova: any;
declare var window: any;
declare var  OTPAutoVerification: any;
/**
 * Generated class for the CheckotpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkotp',
  templateUrl: 'checkotp.html',
})
export class CheckotpPage {
      otpfilter =  /^\d{6}$/;
      public smses:any;
      loader: any;
      account: { otp_no: string, contact_no: string, user_name: string } = {
            otp_no: '',
            contact_no: '',
            user_name: ''
      };
      module: any = null;
  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform,
    public user: User,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController)   {
    if(this.navParams.get("module")!=null){
        this.module= this.navParams.get("module");
    }
    if(this.navParams.get("account")!=null){
        this.account= this.navParams.get("account");
    }else
        this.navCtrl.setRoot('WelcomePage');
//    this.account.contact_no = this.contact_no;
//    this.account.user_name =  this.navParams.get("account").name;
//platform.ready().then(() => { 
//    var permissions = cordova.plugins.permissions;
//    permissions.hasPermission(permissions.READ_SMS, checkPermissionCallback, null);
//    this.getSMS();
//    
//    function checkPermissionCallback(status) {
//        if(!status.hasPermission) {
//        var errorCallback = function() {
//        alert('READ_SMS permission is not turned on');
//        }
//
//        permissions.requestPermission(
//        permissions.READ_SMS,
//        function(status) {
//        if(!status.hasPermission) {
//        errorCallback();
//        }
//        },
//        errorCallback);
//        }
//    }
//});
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckotpPage');
  }
  getSMS(){
       var filter = {
            box : 'inbox', // 'inbox' (default), 'sent', 'draft', 'outbox', 'failed', 'queued', and '' for all
            // following 4 filters should NOT be used together, they are OR relationship
//            read : 0, // 0 for unread SMS, 1 for SMS already read
//            _id : 'ID-VIBRNT', // specify the msg id
//            address : 'VIBRNT', // sender's phone number
//            body : 'Greetings from Prabhat Digital Your 6 digit code is ', // content to match
//            
//            // following 2 filters can be used to list page up/down
//            indexFrom : 0, // start from index 0
            maxCount : 1000, // count of SMS to return each time
          };
          
          
    setTimeout(()=>{
        if(window.SMS) window.SMS.listSMS(filter ,data=>{
                this.smses=data;
//                for (let key in data) {
//                    let value = data[key];
//                    alert(JSON.stringify(value))
//                }
        },error=>{
          alert(error);
          console.log(error);
        });
    },1000)
    
     var options = {
        delimiter : "Greetings from Prabhat Digital Your 6 digit code is ",
        length : 6,
        origin : "VIBRNT"
      };
      
      var success = function (otp) {
        alert("GOT OTP"+otp);
        OTPAutoVerification.stopOTPListener();
      }

      var failure = function () {
          alert("Problem in listening OTP");
        OTPAutoVerification.stopOTPListener();
        
      }
      
      OTPAutoVerification.startOTPListener(options, success, failure);
      
  }
  otpCheck(){
    // Attempt to login in through our User service
      if (this.account.otp_no==""){
          this.showError("Please Provide Valid Otp.");
      } else if (this.account.otp_no== "" || this.account.otp_no.length!=6){
          this.showError("Please Provide Valid Otp Number.");
      }else{
          this.presentLoading();
      }
  }
  checkOtp(){
      this.user.checkOtp(this.account).map(res => res.json())
        .subscribe(res => {
            this.closeLoading();
            if (res) {
                if(this.module==null){
                    this.showFullRegistration();
                }else{
                    this.navCtrl.pop();
                    this.module.updAcoountDetails();
                }
                
            } else {
                this.showError("Otp Does not match please try agian.");
            }
          }, err => {
              this.closeLoading();
              alert(err);
          });
  }
  showError(message){
      let toast = this.toastCtrl.create({
        message: message,
        duration: 3000,
        position: 'top'
      });
      toast.present();
  }
  showFullRegistration(){
    this.navCtrl.setRoot('Signup1Page',{'account':this.account} ,{
      animate: true,
      direction: 'forward'
    });
  }
presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    this.loader=loader; 
    this.checkOtp();
  }
  closeLoading(){
      this.loader.dismiss();
  }  

}
