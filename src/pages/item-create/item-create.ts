import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Camera } from '@ionic-native/camera';
import { IonicPage, NavController, ViewController, ToastController, ItemSliding } from 'ionic-angular';
import {OrderProductProvider} from "../../providers/order-product/order-product";

@IonicPage()
@Component({
  selector: 'page-item-create',
  templateUrl: 'item-create.html'
})
export class ItemCreatePage {
 order_products: any[];
  logins: any[];
  no_of_item: number=0;
  total: number=0;
  total_discount: number=0;
  total_estimate: number=0;
  

  constructor(private toastCtrl: ToastController,public orderProduct:OrderProductProvider,public viewCtrl: ViewController,public navCtrl: NavController) {
      this.getProduct();
  }

getProduct(){
    this.no_of_item=0;
    this.total=0;
    this.total_discount=0;
    this.total_estimate=0;
    this.order_products = this.orderProduct.selectAllOrderProduct();
    this.no_of_item = this.order_products.length;
    for (let value in this.order_products) {
        this.total= this.total + parseInt(this.order_products[value].service_charge);
    }
    this.total_estimate = this.total;
}
  continue(){
      this.navCtrl.push('SelectAddressPage', {'no_of_item': this.no_of_item});
  }
  delete(item: ItemSliding, index_no: any) {
    this.orderProduct.deleteOrderProduct(index_no);
    this.getProduct();
  }
  cancel() {
    this.viewCtrl.dismiss();
  }
}
