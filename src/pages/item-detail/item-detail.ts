import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ViewController, PopoverController, ToastController } from 'ionic-angular';

import { Items } from '../../providers/providers';
import {ProblemDetailsProvider} from '../../providers/problem-details/problem-details';
import {OrderProductProvider} from '../../providers/order-product/order-product';
import {PopoverComponent} from '../../components/popover/popover';

@IonicPage()
@Component({
  selector: 'page-item-detail',
  templateUrl: 'item-detail.html'
})


export class ItemDetailPage {
  brand_details_id: any=0;
  model_details_id: any=0;
  problem_details_id: any=0;
  ITEM_DETAILS_ADD_MORE: boolean = false;
  add_to_cart_color: boolean=true;
  hide_discription: boolean=false;
  hide_brand_details: boolean=false;
  hide_model_details: boolean=false;
  hide_problem_details: boolean=false;
  problem_discription: string="";
  discription: string="";
  item: any;
  brand_details: any;
  model_details: any;
  problem_details: any;
  order_products: any=[];
  loader: any;
  brand_detail_ngModel: string;
  model_details_ngModel: string;
  problem_details_ngModel: string;
  brand_detailAlertOpts: { title: string, subTitle: string };
  model_detailsAlertOpts: { title: string, subTitle: string };
  problem_detailsAlertOpts: { title: string, subTitle: string };
  constructor(public navCtrl: NavController, navParams: NavParams, items: Items,public problemDetails:ProblemDetailsProvider,public loadingCtrl: LoadingController,public orderProduct:OrderProductProvider,public popoverCtrl: PopoverController,public toastCtrl: ToastController) {
    this.item = navParams.get('item') || items.defaultItem;
    this.presentLoading();
    this.getProduct();
  }
  presentPopover(myEvent) {
    let that=this;  
    let popover = this.popoverCtrl.create(PopoverComponent,{
      getItem: function() {
          return that.item;
      }});
    popover.present({
      ev: myEvent
    });
  }
  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    this.loader=loader; 
    this.loadProblemDetails();
    
  }
  closeLoading(){
      this.loader.dismiss();
  }
  loadProblemDetails(){
      this.brand_detailAlertOpts= {
        title: 'Brand Details',
        subTitle:'Select Your Product Brand'
      }
      this.problemDetails.loadProblemDetails(this.item.product_details_id,this);
      
  }
  addBrandDetails(brand_details:any[]){
      if (brand_details.length>0){
          this.hide_brand_details=!this.hide_brand_details;
      }
      this.brand_details=brand_details;
      this.selectModel(0);
  }
  setDefault(){
      this.model_details_id=0;
      this.problem_details_id=0;
      this.hide_problem_details=false;
      this.hide_model_details=false;
      this.hide_discription=false;
      this.problem_discription="";
  }
  addModelDetails(model_details:any[]){
      this.setDefault();
      if (model_details.length>0){
          this.hide_model_details=!this.hide_model_details;
      }
      this.model_detailsAlertOpts= {
        title: 'Model Details',
        subTitle:'Select Your Product Model'
      }
      this.model_details=model_details;
  }
  addProblemDetails(problem_details:any[]){
      if (problem_details.length>0){
          this.hide_problem_details=!this.hide_problem_details;
      }
      this.problem_detailsAlertOpts= {
        title: 'Problem Details',
        subTitle:'Select Your Product Problem'
      }
      this.problem_details=problem_details;
  }
  selectBrand(brand_details_id: any){
      this.brand_details_id = brand_details_id;
      this.problemDetails.loadModelDetails(brand_details_id,this);
  }
  selectModel(model_details_id: any){
      this.model_details_id = model_details_id;
      this.problemDetails.loadProblem(model_details_id,this);
  } 
  selectProblem(problem_details_id: any){
      this.problem_details_id = problem_details_id;
      this.hide_discription=!this.hide_discription;
      for (let key of this.problem_details) {
          if (key["problem_details_id"] == problem_details_id){
              this.discription= key["discription"]
              break;
          }
      }
  } 
  addProduct(){
   if(this.add_to_cart_color){
      if (this.problem_details_id != 0 || this.problem_discription!="" ){
          this.add_to_cart_color=!this.add_to_cart_color;
          this.ITEM_DETAILS_ADD_MORE=!this.ITEM_DETAILS_ADD_MORE;
          this.setProduct();
      }else{
          this.presentToast("Kindly Provide Valid Information about Item.");
      }
    }else{
          this.removeProduct();
          this.add_to_cart_color=!this.add_to_cart_color;
    }
  }
  setProduct(){
    let order_product={
        product_details_id:this.item.product_details_id,
        icon:this.item.icon,
        service_charge:this.item.service_charge,
        brand_details_id:this.brand_details_id!=0?this.brand_details_id:"",
        model_details_id:this.model_details_id!=0?this.model_details_id:"",
        model_name:this.model_details_id!=0?this.getModelName(this.model_details_id):"",
        problem_details_id:this.problem_details_id!=0?this.problem_details_id:"",
        problem_discription:this.problem_discription,
        discription:this.discription,
    }
    this.orderProduct.insertOrderProduct(order_product);
    this.getProduct();
  }
  getModelName(model_details_id: any){
      for (let item of this.model_details) {
        if(item.model_details_id==model_details_id){
            return item.model_name;
        }
      }
      return "";
  }
  
  getProduct(){
      this.order_products = this.orderProduct.selectOrderProduct(this.item.product_details_id);
  }
  removeProduct(){
      let order_product={
        product_details_id:this.item.product_details_id,
        brand_details_id:this.brand_details_id=!0?this.brand_details_id:"",
        model_details_id:this.model_details_id=!0?this.model_details_id:"",
        problem_details_id:this.problem_details_id=!0?this.problem_details_id:"",
        problem_discription:this.problem_discription,
      }
      this.orderProduct.deleteOrderProduct(order_product);
      this.getProduct();
  }
  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }
  addMore(){
      this.navCtrl.pop();
      this.navCtrl.push('ItemDetailPage', {
       item: this.item
      });
  }
}
