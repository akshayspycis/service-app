import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController } from 'ionic-angular';

import { Item } from '../../models/item';
import { Items, User } from '../../providers/providers';
import { Http } from '@angular/http';
import { url } from '../pages';
import {ProductDetailsProvider} from '../../providers/product-details/product-details';
import {OrderProductProvider} from "../../providers/order-product/order-product";
@IonicPage()
@Component({
  selector: 'page-list-master',
  templateUrl: 'list-master.html'
})
export class ListMasterPage {
  currentItems: Item[];
  product_details:any=[];
  pic: string;
  user_name: string;
  constructor(public navCtrl: NavController, public items: Items, public modalCtrl: ModalController,public http: Http,public productDetails :ProductDetailsProvider,public orderProduct:OrderProductProvider,public user:User) {
      
      
    this.currentItems = this.items.query();
    this.loadProductDetails();
    if(this.user.getUser().pic!=null)
    this.pic = url+ this.user.getUser().pic.substring(3, this.user.getUser().pic.length);
    else{
        this.pic='assets/img/header-profile-default.png';
    }
    this.user_name = this.capitalizeFirstLetter(this.user.getUser().user_name);
  }
  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
    loadProductDetails(){
        this.productDetails.loadProductDetails().subscribe((data)=>{
            this.product_details = data;
        });
    }
    loadForcely(refresher){
         setTimeout(() => {
             this.productDetails.loadForcely().subscribe((data)=>{
                this.product_details = data;
                refresher.complete();
             });
         },2000);
    }
  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
  }

  /**
   * Prompt the user to add a new item. This shows our ItemCreatePage in a
   * modal and then adds the new item to our data source if the user created one.
   */
  addItem() {
//    this.navCtrl.push('ItemCreatePage');
      let addModal = this.modalCtrl.create('ItemCreatePage');
    addModal.onDidDismiss(item => {
      if (item) {
        this.items.add(item);
      }
    })
    addModal.present();
  }

  /**
   * Delete an item from the list of items.
   */
  deleteItem(item) {
    this.items.delete(item);
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Item) {
    this.navCtrl.push('ItemDetailPage', {
      item: item
    });
  }
}
