import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController, NavParams, LoadingController, MenuController } from 'ionic-angular';

import { User } from '../../providers/providers';
import { MainPage } from '../pages';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  // The account fields for the login form.
  // If you're using the username field with or without contact_no, make
  // sure to add it to the type
  loader: any;  
  account: { contact_no: string, password: string } = {
    contact_no: '9876543210',
    password: '1234qwer'
  };

  // Our translated text strings
  private loginErrorString: string;

  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public translateService: TranslateService
    ,public menu: MenuController) { 
      
    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })
  }

  // Attempt to login in through our User service
  doLogin() {
      this.user.login(this.account).map(res => res.json()).subscribe((res) => {
        this.closeLoading();
        if (res.status==null) {
            this.user._loggedIn(res);
            this.menu.enable(true);
            if(res.pic==null){
                this.navCtrl.setRoot('ProfilepicPage');
            }else{
            
                this.navCtrl.setRoot(MainPage);
            }
        } else {
            this.showError("Invalid User Name & Password.");
        }
      }, err => {
          this.closeLoading();
          alert("error "+err);
      });
  }
  login(){
      this.presentLoading();
  }
presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    this.loader=loader; 
    this.doLogin();
  }
  closeLoading(){
      this.loader.dismiss();
  }
  showError(message){
      let toast = this.toastCtrl.create({
        message: message,
        duration: 5000,
        position: 'top'
      });
      toast.present();
  }
}
