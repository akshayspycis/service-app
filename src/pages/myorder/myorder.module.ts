import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MyorderPage } from './myorder';

@NgModule({
  declarations: [
    MyorderPage,
  ],
  imports: [
    IonicPageModule.forChild(MyorderPage),
    TranslateModule.forChild()
  ],
})
export class MyorderPageModule {}
