import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import {TranslateService} from "@ngx-translate/core";
import {User} from "../../providers/providers";
import {OrderProvider} from "../../providers/order/order";

/**
 * Generated class for the MyorderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-myorder',
  templateUrl: 'myorder.html',
})
export class MyorderPage {
private signupErrorString: string;
loader: any;
order_details: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public translateService: TranslateService,public toastCtrl: ToastController,public loadingCtrl: LoadingController,public user: User,public order:OrderProvider) {
   this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
   if (this.user.getUser() == undefined){
       this.navCtrl.push("WelcomePage");
       return ;
   }
   this.presentLoading();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MyorderPage');
  }
  selOrder(){
      this.order.loadOrder().subscribe((data)=>{
          this.closeLoading();
          this.order_details = data;
      });
  }
  openOrder(order_detail){
      this.navCtrl.push("OrderProductPage",{'order_detail':order_detail})
  }
  showError(message){
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
      position: 'top'
    });
    toast.present();
  }
  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    this.loader=loader; 
    this.selOrder();
  }
  closeLoading(){
      this.loader.dismiss();
  }
  loadForcely(refresher){
         setTimeout(() => {
             this.order.loadForcely().subscribe((data)=>{
                this.order_details = data;
                refresher.complete();
             });
         },1000);
    }
}
