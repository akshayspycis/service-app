import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderProductPage } from './order-product';

@NgModule({
  declarations: [
    OrderProductPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderProductPage),
    TranslateModule.forChild()
  ],
})
export class OrderProductPageModule {}
