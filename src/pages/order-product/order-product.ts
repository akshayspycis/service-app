import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import {TranslateService} from "@ngx-translate/core";
import {User} from "../../providers/providers";
import {OrderProvider} from "../../providers/order/order";

/**
 * Generated class for the OrderProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-order-product',
  templateUrl: 'order-product.html',
})
export class OrderProductPage {

private signupErrorString: string;
loader: any;
order_no: string='';
order_products: any;
order_detail: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public translateService: TranslateService,public toastCtrl: ToastController,public loadingCtrl: LoadingController,public user: User,public order:OrderProvider) {
    if (this.navParams.get('order_detail')!=null){
        this.order_detail=this.navParams.get('order_detail');
        this.order_no = this.order_detail.order_no;
    }
   this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
    
   if (this.user.getUser() == undefined){
       this.navCtrl.push("WelcomePage");
       return ;
   }
   
   this.presentLoading();
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderProductPage');
  }
  selOrderProduct(){
      this.order.loadOrderProduct(this.order_detail).subscribe((data)=>{
          this.closeLoading();
          this.order_products = data;
      });
  }
  showError(message){
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
      position: 'top'
    });
    toast.present();
  }
  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    this.loader=loader; 
    this.selOrderProduct();
  }
  closeLoading(){
      this.loader.dismiss();
  }
  openPrice(order_product){
      this.navCtrl.push('PriceDetailsPage',{'order_detail':this.order_detail,'order_product':order_product});
  }
  openRepaire(order_product){
      this.navCtrl.push('RepairDetailsPage',{'order_detail':this.order_detail,'order_product':order_product});
  }
}
