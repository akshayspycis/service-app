import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderdonePage } from './orderdone';

@NgModule({
  declarations: [
    OrderdonePage,
  ],
  imports: [
    IonicPageModule.forChild(OrderdonePage),
  ],
})
export class OrderdonePageModule {}
