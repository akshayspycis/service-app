import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { MainPage } from '../pages';
/**
 * Generated class for the OrderdonePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-orderdone',
  templateUrl: 'orderdone.html',
})
export class OrderdonePage {
  order_no: string;
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {
      this.navParams.get("order_no")!=null?this.order_no= this.navParams.get("order_no"):'';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderdonePage');
  }
  done(){
      this.viewCtrl.dismiss();
//    this.navCtrl.push(MainPage);
  }

}
