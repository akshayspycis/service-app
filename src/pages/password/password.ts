import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import {User} from "../../providers/providers";

/**
 * Generated class for the PasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-password',
  templateUrl: 'password.html',
})
export class PasswordPage {

  check_email=false;    
  account: {password: string, confirm_password: string, user_id: string } = {
            password: '',
            confirm_password: '',
            user_id:''
      };
  loader: any;  
  check_condition=true;
  constructor(public navCtrl: NavController, public navParams: NavParams,public user: User,public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
      if (this.user.getUser()==null){
          this.navCtrl.push("WelcomePage");
      }
  }
  passwordfilter = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
  updUserPassword(){
      this.account.user_id = this.user.getUser().user_id;
      this.user.updUserPassword(this.account).map(res => res.json()).subscribe((res) => {
        this.closeLoading();
        if (res) {
            this.showError("Password updated Succesfully.");
            this.user.logout();
            this.navCtrl.push("WelcomePage");
        } else {
            this.showError("Internal Server Error.");
        }
      }, err => {
          alert("error "+err);
      });
  }
  checkSamePassword(){
      if(this.account.password!=this.account.confirm_password){
          this.showError("Your password does not match.");
      }
  }
  showCondition(){
      if(this.check_condition){
          alert("Your Password must contain Minimum eight characters, at least one letter and one number");
          this.check_condition = false;
      }
  }

  showError(message){
      let toast = this.toastCtrl.create({
        message: message,
        duration: 5000,
        position: 'top'
      });
      toast.present();
  }
   presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    this.loader=loader; 
    this.updUserPassword();
  }
  closeLoading(){
      this.loader.dismiss();
  }
  submitRegistration(){
      this.presentLoading() ;
  }

}
