import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
    
/**
 * Generated class for the PaymentOptionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment-options',
  templateUrl: 'payment-options.html',
})
export class PaymentOptionsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private payPal: PayPal) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentOptionsPage');
  }
  pay(){
      console.log("call Pay")
      let _self=this;
      console.log("call init")
      this.payPal.init({
  PayPalEnvironmentProduction: 'Adu0ykNBwdmO7akJB8vaZWvRpe-xUhTtui1mCYa7OPhxyR77Z-ikV4eNzP7KwCCLFaF3frsPhqz6Or6C',
  PayPalEnvironmentSandbox: 'ARfBhXnnGfWQuGLJie-yqt4Qi8bftae_qmq2HsXehOX4AKi6q0W9oWrUhrTVNJyKrVfwGwkyyBjK2v4z'
}).then(() => {
console.log("su init")
  // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
  this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
    // Only needed if you get an "Internal Service Error" after PayPal login!
    //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
  })).then(() => {
    let payment = new PayPalPayment('33', 'INR', 'Description', 'sale');
    this.payPal.renderSinglePaymentUI(payment).then((e) => {
        console.log("renderSinglePaymentUI",e)
      // Successfully paid

      // Example sandbox response
      //
      // {
      //   "client": {
      //     "environment": "sandbox",
      //     "product_name": "PayPal iOS SDK",
      //     "paypal_sdk_version": "2.16.0",
      //     "platform": "iOS"
      //   },
      //   "response_type": "payment",
      //   "response": {
      //     "id": "PAY-1AB23456CD789012EF34GHIJ",
      //     "state": "approved",
      //     "create_time": "2016-10-03T13:33:33Z",
      //     "intent": "sale"
      //   }
      // }
    }, (e) => {
   console.log('renderSinglePaymentUI',e);
      // Error or render dialog closed without being successful
    });
  }, (e) => {
   console.log('prepareToRender',e);
    // Error in configuration
  });
}, (e) => {
   console.log('init',e);
  // Error in initialization, maybe PayPal isn't supported or something else
});
  }

}
