import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { PriceDetailsPage } from './price-details';

@NgModule({
  declarations: [
    PriceDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PriceDetailsPage),
    TranslateModule.forChild()
  ],
})
export class PriceDetailsPageModule {}
