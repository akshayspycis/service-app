import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import {TranslateService} from "@ngx-translate/core";
import {User} from "../../providers/providers";
import {OrderProvider} from "../../providers/order/order";
import {PaytmPaymentGatewayProvider} from "../../providers/paytm-payment-gateway/paytm-payment-gateway";
declare var paytm : any;
/**
 * Generated class for the PriceDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-price-details',
  templateUrl: 'price-details.html',
})
export class PriceDetailsPage {
signupErrorString: any;
loader: any;
order_no: string='';
order_product: any;
order_detail: any;
payment_details: any;
count: number=0;
total: number=0;
  constructor(public navCtrl: NavController, public navParams: NavParams,public translateService: TranslateService,public toastCtrl: ToastController,public loadingCtrl: LoadingController,public user: User,public order:OrderProvider,public paytm_payment_gateway_provider:PaytmPaymentGatewayProvider) {
    if (this.navParams.get('order_detail')!=null){
        this.order_detail=this.navParams.get('order_detail');
        this.order_no = this.order_detail.order_no;
    }
    if (this.navParams.get('order_product')!=null){
        this.order_product=this.navParams.get('order_product');
    }
   this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
    
   if (this.user.getUser() == undefined){
       this.navCtrl.push("WelcomePage");
       return ;
   }
   
   this.presentLoading();
}


  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderProductPage');
  }
  selPaymentDetails(){
      this.order.loadPaymentDetails(this.order_product).subscribe((data)=>{
          this.closeLoading();
          this.payment_details = data;
          for (let item of this.payment_details) {
            if(item.type!=null&&item.type=='M'){
                this.count = this.count - parseFloat(item.amount)
            }else{
                this.count=this.count+parseFloat(item.amount)
            }  
          }
          this.total = this.count +this.count*18/100;
      });
      
  }
  showError(message){
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
      position: 'top'
    });
    toast.present();
  }
  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    this.loader=loader; 
    this.selPaymentDetails();
  }
  closeLoading(){
      this.loader.dismiss();
  }
  openPrice(order_product){
      
  }
 
  payPayment(CHECK_SUMHASH){
      var  options={
  "REQUEST_TYPE": "DEFAULT",
  "MID": "PRABHA29384309714042",
  "ORDER_ID": "ORDER0003INFOPARK",
  "CUST_ID": "C1INFOPARK",
  "INDUSTRY_TYPE_ID": "Retail",
  "CHANNEL_ID": "WAP",
  "TXN_AMOUNT": "10.00",
  "WEBSITE": "APPSTAGING",
  "CALLBACK_URL": "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=",
  "EMAIL": "",
  "MOBILE_NO": "",
  "CHECKSUMHASH": CHECK_SUMHASH
}
      console.log(JSON.stringify(options))
      paytm.startPayment(options, this.successCallback, this.failureCallback);
  }
  successCallback(response) {
    if (response.STATUS == "TXN_SUCCESS") {
        var txn_id = response.TXNID;
        var paymentmode = response.PAYMENTMODE;
    } else {
        alert("Transaction Failed for reason " + response.RESPMSG);
    }
}
  failureCallback(error) {
      console.log(JSON.stringify(error))
    // error code will be available in RESCODE
    // error list page https://docs.google.com/spreadsheets/d/1h63fSrAmEml3CYV-vBdHNErxjJjg8-YBSpNyZby6kkQ/edit#gid=2058248999
    console.log("Transaction Failed for reason " + error.RESPMSG);
  }
  getCheckShum(){
      this.paytm_payment_gateway_provider.getCheckSum(null).map(res => res.json()).subscribe((res) => {
          console.log(res.CHECKSUMHASH)
          this.payPayment(res.CHECKSUMHASH);
      }, err => {
          alert("error "+err);
      });
  }
}
