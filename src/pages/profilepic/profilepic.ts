import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Camera } from '@ionic-native/camera';
import { IonicPage, NavController, ViewController, LoadingController, Platform, ToastController, ActionSheetController, Loading, MenuController } from 'ionic-angular';
import { File } from '@ionic-native/file';

import { FilePath } from '@ionic-native/file-path';
import { DomSanitizer } from '@angular/platform-browser';
import {User} from "../../providers/providers";
declare var cordova: any;
declare var window: any;
import {MainPage, url } from '../pages';

/**
 * Generated class for the ProfilepicPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profilepic',
  templateUrl: 'profilepic.html',
})

export class ProfilepicPage
     {
  @ViewChild('fileInput') fileInput;
  isReadyToSave: boolean;
  item: any;
  form: FormGroup;
  lastImage: string = null;
  loading: Loading;
  loader: any;    
  account: {pic: string, user_id: string} = {
            pic: '',
            user_id:''
      };
 constructor(private _sanitizer: DomSanitizer,public navCtrl: NavController, public viewCtrl: ViewController, formBuilder: FormBuilder, public camera: Camera, private file: File, 
     private filePath: FilePath, public actionSheetCtrl: ActionSheetController, public toastCtrl: ToastController, public platform: Platform, public loadingCtrl: LoadingController,public user: User,public menu: MenuController) { 
      this.menu.enable(true);
      this.form = formBuilder.group({
          lastImage: [''],
      });
    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
    
//    if(this.user.getUser()==null)this.navCtrl.push("WelcomePage");
//    this.lastImage = this.user.getUser().pic == null?null:url+ this.user.getUser().pic.substring(3, this.user.getUser().pic.length);
  }
  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }
public takePicture(sourceType) {
  // Create options for the Camera Dialog
  var options = {
    quality: 100,
    sourceType: sourceType,
    saveToPhotoAlbum: false,
    correctOrientation: true
  };
 
  // Get the data of an image
  this.camera.getPicture(options).then((imagePath) => {
  // Special handling for Android library
    if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
      this.filePath.resolveNativePath(imagePath)
        .then(filePath => {
          let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
          let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
          this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
        });
    } else {
          var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
          var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
          this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
    }
  }, (err) => {
    this.presentToast(err);
  });
}
private createFileName() {
  var d = new Date(),
  n = d.getTime(),
  newFileName =  n + ".jpg";
  return newFileName;
}
private copyFileToLocalDir(namePath, currentName, newFileName) {
  this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
    this.lastImage = newFileName;
  }, error => {
    this.presentToast('Error while storing file.');
  });
}
private presentToast(text) {
  let toast = this.toastCtrl.create({
    message: text,
    duration: 3000,
    position: 'top'
  });
  toast.present();
}
public pathForImage(img) {
  if (img === null) {
    return '';
  } else {
    var urla="url('"+cordova.file.dataDirectory + img+"')";
    this.processWebImage(cordova.file.dataDirectory + img);
    return  this._sanitizer.bypassSecurityTrustStyle(urla);
//    return cordova.file.dataDirectory + img;
    
  }
}
public uploadImage() {
  this.account.user_id = this.user.getUser()==null?'0':this.user.getUser().user_id;  
  this.user.insUserPic(this.account).map(res => res.json()).subscribe((res) => {
        this.closeLoading();
        if (res.pic!="null") {
            var a = this.user.getUser();
            a.pic=res.pic;
            this.user._loggedIn(a);
            this.navCtrl.setRoot(MainPage);
        } else {
            this.showError("Internal Server Error.");
        }
      }, err => {
          alert(err);
          this.closeLoading();
     });
}

getPicture() {
      this.presentActionSheet();
//    if (Camera['installed']()) {
//      this.camera.getPicture({
//        destinationType: this.camera.DestinationType.DATA_URL,
//        targetWidth: 96,
//        targetHeight: 96
//      }).then((data) => {
//        this.form.patchValue({ 'lastImage': 'data:image/jpg;base64,' + data });
//      }, (err) => {
//        alert('Unable to take photo');
//      })
//    } else {
//      this.fileInput.nativeElement.click();
//    }
  }

  processWebImage(event) {
     var that =this;
     this.getFileContentAsBase64(event,function(base64Image){
        //window.open(base64Image);
        that.account.pic=base64Image;
        // Then you'll be able to handle the myimage.png file as base64
    });
  }


getFileContentAsBase64(path,callback){
    window.resolveLocalFileSystemURL(path, gotFile, fail);
    function fail(e) {
          alert('Cannot found requested file');
    }

    function gotFile(fileEntry) {
           fileEntry.file(function(file) {
              var reader = new FileReader();
              reader.onloadend = function(e) {
                   var content = this.result;
                   callback(content);
              };
              // The most important point, use the readAsDatURL Method from the file plugin
              reader.readAsDataURL(file);
           });
    }
}
  getProfileImageStyle() {
    return 'url(' + this.form.controls['lastImage'].value + ')'
  }

  /**
   * The user cancelled, so we dismiss without sending data back.
   */
  upload() {
    this.presentLoading();
  }

  /**
   * The user is done and wants to create the item, so return it
   * back to the presenter.
   */
  skip() {
      this.navCtrl.setRoot(MainPage);
  }
    
  showError(message){
      let toast = this.toastCtrl.create({
        message: message,
        duration: 5000,
        position: 'top'
      });
      toast.present();
  }
   presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    this.loader=loader; 
    this.uploadImage();
  }
  closeLoading(){
      this.loader.dismiss();
  }
  
}
