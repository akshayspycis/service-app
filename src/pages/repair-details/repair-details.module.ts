import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { RepairDetailsPage } from './repair-details';

@NgModule({
  declarations: [
    RepairDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(RepairDetailsPage),
    TranslateModule.forChild()
  ],
})
export class RepairDetailsPageModule {}
