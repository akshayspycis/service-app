import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import {TranslateService} from "@ngx-translate/core";
import {User} from "../../providers/providers";
import {OrderProvider} from "../../providers/order/order";

/**
 * Generated class for the RepairDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-repair-details',
  templateUrl: 'repair-details.html',
})
export class RepairDetailsPage {
private signupErrorString: string;
loader: any;
order_no: string='';
order_product: any;
order_detail: any;
repair_details: any;
account: {order_lab_extra_charge_id: string, order_lab_spare_id: string, status: string} = {
            order_lab_extra_charge_id: '',
            order_lab_spare_id: '',
            status: 'true'
      };
check_resolve: boolean= false;      
resolve: {title: string, dae: string, message: string,l:string} = {
            title: 'Resolve Issue',
            dae: '',
            message: '',
            l:''
};
  constructor(public navCtrl: NavController, public navParams: NavParams,public translateService: TranslateService,public toastCtrl: ToastController,public loadingCtrl: LoadingController,public user: User,public order:OrderProvider) {
    if (this.navParams.get('order_detail')!=null){
        this.order_detail=this.navParams.get('order_detail');
        this.order_product=this.navParams.get('order_product');
        this.order_no = this.order_detail.order_no;
    }
   this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
   if (this.user.getUser() == undefined){
       this.navCtrl.push("WelcomePage");
       return ;
   }
   this.presentLoading();
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderProductPage');
  }
  selRepairDetails(){
      this.order.loadRepairDetails(this.order_product).subscribe((data)=>{
          this.closeLoading();
          this.repair_details = data;
          for (let item of this.repair_details) {
            if(item.type=='OP'&&item.status==3){
                this.check_resolve=true;
                this.resolve.dae = item.dae;
                this.resolve.message = item.message;
                this.resolve.l = this.repair_details.length+2;
            }
          }
      });
  }
  showError(message){
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
      position: 'top'
    });
    toast.present();
  }
  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    this.loader=loader; 
    this.selRepairDetails();
  }
  closeLoading(){
      this.loader.dismiss();
  }
  openPrice(order_product){
      this.navCtrl.push('PriceDetailsPage',{'order_detail':this.order_detail,'order_product':order_product});
  }
  givePermission(status,type,id){
      if(status=='false')
      type=='OL'?this.updOrderLabExtraChargeStatus(id):this.updOrderLabSpareStatus(id);
  }
  updOrderLabExtraChargeStatus(id){
      this.account.order_lab_extra_charge_id = id;
      this.order.updOrderLabExtraChargeStatus(this.account).subscribe((data)=>{
         if (data) {
            this.presentLoading();
         } else {
            this.showError("Internal Server Error.");
         }
      }, err => {
          alert("error "+err);
      });
  }
  updOrderLabSpareStatus(id){
        this.account.order_lab_spare_id = id;
        this.order.updOrderLabSpareStatus(this.account).subscribe((data)=>{
         if (data) {
            this.presentLoading();
         } else {
            this.showError("Internal Server Error.");
         }
        }, err => {
            alert("error "+err);
        });
  }
  
}
