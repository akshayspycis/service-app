import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectAddressPage } from './select-address';

@NgModule({
  declarations: [
    SelectAddressPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectAddressPage),
    TranslateModule.forChild()
  ],
})
export class SelectAddressPageModule {}
