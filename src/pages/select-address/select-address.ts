import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import {TranslateService} from "@ngx-translate/core";
import {AddressDetailsProvider} from "../../providers/address-details/address-details";

/**
 * Generated class for the SelectAddressPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-select-address',
  templateUrl: 'select-address.html',
})
export class SelectAddressPage {
address_details: any;    
loader: any;
no_of_item: number=0;
private signupErrorString: string;
  constructor(public navCtrl: NavController, public navParams: NavParams,public translateService: TranslateService,public address_details_provider:AddressDetailsProvider,public toastCtrl: ToastController,public loadingCtrl: LoadingController) {
    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
    try {
        
        this.presentLoading();
    } catch (e) {
        this.navCtrl.setRoot('WelcomePage');
    }
    this.no_of_item=this.navParams.get('no_of_item')!=null?this.navParams.get('no_of_item'):0;
  }
  
  loadAddressDetails(){
      this.address_details_provider.loadAddressDetails().subscribe((data)=>{
            this.closeLoading();
            this.address_details = data;
      });
  }
  showEdit(address_details_id){
      for (let address_detail of this.address_details) {
             if(address_detail.address_details_id==address_details_id)
                address_detail.edit=true;
             else
                address_detail.edit=false;
      }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectAddressPage');
  }
  addAddress(){
      this.navCtrl.push('AddAddressPage',{'obj':this});
  }
  editAddress(address_detail){
      this.navCtrl.push('AddAddressPage', {'account': address_detail});
  }
  deleteAddress(address_detail){
      this.address_details_provider.deleteAddressDetails(address_detail).subscribe((data)=>{
          this.presentLoading();
      });
  }
  continue(){
      var address_details_id='';
      for (let address_detail of this.address_details) {
         if(address_detail.edit==true)
            address_details_id=address_detail.address_details_id;
      }
      if(address_details_id!=''){
          this.navCtrl.push('AltcontactPage', {'address_details_id': address_details_id});
      }else{
          this.showError('Please Select any address.');
      }
  }
  showError(message){
      let toast = this.toastCtrl.create({
        message: message,
        duration: 3000,
        position: 'top'
      });
      toast.present();
  }
   presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    this.loader=loader; 
    this.loadAddressDetails();
  }
  closeLoading(){
      this.loader.dismiss();
  }
  loadForce(){
      this.loadAddressDetails();
  }

}
