import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController, LoadingController } from 'ionic-angular';

import { User } from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  contactfilter =  /^\d{10}$/;
  loader: any;
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: {user_name: string, contact_no:string} = {
    user_name: '',
    contact_no: '',
  };
  // Our translated text strings
  private signupErrorString: string;
  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public translateService: TranslateService) {
    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
  }
  
  checkContactNo(){
    // Attempt to login in through our User service
      if (this.account.user_name==""){
          this.showError("Please Provide Valid Name.");
      } else if (this.account.contact_no == "" ||  !this.contactfilter.test(this.account.contact_no)){
          this.showError("Please Provide Valid Contact Number.");
      }else{
        this.presentLoading();
      }
  }
  getOtp(){
      this.user.getOtp(this.account).map(res => res.json()).subscribe((res) => {
          this.closeLoading();
            if(!res){
                this.showError("This Contact no is already exits.");
            }else{
                this.navCtrl.push('CheckotpPage', {'account': this.account});
            }
        }, (err) => {
            this.closeLoading();
            this.showError("Internal Server Error");
        });
  }
  showError(message){
      let toast = this.toastCtrl.create({
        message: message,
        duration: 3000,
        position: 'top'
      });
      toast.present();
  }
   presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    this.loader=loader; 
    this.getOtp();
  }
  closeLoading(){
      this.loader.dismiss();
  }
  
}
