import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { Signup1Page } from './signup1';

@NgModule({
  declarations: [
    Signup1Page,
  ],
  imports: [
    IonicPageModule.forChild(Signup1Page),
    TranslateModule.forChild()
  ],
  exports: [
    Signup1Page
  ]
})
export class Signup1PageModule {}
