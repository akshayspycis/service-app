import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import {FormBuilder} from "@angular/forms/src/form_builder";
import {User} from "../../providers/providers";
/**
 * Generated class for the Signup1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup1',
  templateUrl: 'signup1.html',
})
export class Signup1Page {
  check_email=false;    
  account: {email: string, password: string, confirm_password: string, user_id: string } = {
            email: '',
            password: '',
            confirm_password: '',
            user_id:''
      };
  loader: any;  
  check_condition=true;
  constructor(public navCtrl: NavController, public navParams: NavParams,public user: User,public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
      if(this.navParams.get("account")!=null){
          this.account = this.navParams.get("account");
          this.account.email='';
          this.account.password='';
          this.account.confirm_password='';
      }else{
          this.navCtrl.push("WelcomePage");
      }
  }
  emailfilter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  passwordfilter = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
  submitRegistration(){
      if (this.account.email == "" || !this.emailfilter.test(this.account.email) || !this.check_email){
          this.showError("Please Provide Valid Email.");
      } else if (this.account.password == "" ||  !this.passwordfilter.test(this.account.password) || this.account.password!=this.account.confirm_password){
          this.showError("Please Provide Valid Password.Your Password must contain Minimum eight characters, at least one letter and one number");
      }else{
          this.presentLoading();
      }
  }
  insUserDetails(){
      this.user.insUserDetails(this.account).map(res => res.json()).subscribe((res) => {
        this.closeLoading();
        if (res) {
            this.account.user_id=res;
            this.user._loggedIn(this.account);
            this.navCtrl.setRoot('ProfilepicPage');
        } else {
            this.showError("Internal Server Error.");
        }
      }, err => {
          alert("error "+err);
      });
  }
  
  
  checkEmail(){
      if (!this.check_email && this.emailfilter.test(this.account.email)){
        this.user.checkEmail(this.account).map(res => res.json()).subscribe((res) => {
            this.check_email=res;
            if(!this.check_email){
                this.showError("This Email Address already exits.");
            }
          }, err => {
              this.closeLoading();
              alert("error"+err);
          });
      }
  }
  checkSamePassword(){
      if(this.account.password!=this.account.confirm_password){
          this.showError("Your password does not match.");
      }
  }
  showCondition(){
      if(this.check_condition){
          alert("Your Password must contain Minimum eight characters, at least one letter and one number");
          this.check_condition = false;
      }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Signup1Page');
  }
  showError(message){
      let toast = this.toastCtrl.create({
        message: message,
        duration: 5000,
        position: 'top'
      });
      toast.present();
  }
   presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    this.loader=loader; 
    this.insUserDetails();
  }
  closeLoading(){
      this.loader.dismiss();
  }

}
