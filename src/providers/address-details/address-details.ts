import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {User} from "../../providers/providers";
import { Api } from '../api/api';
import 'rxjs/add/operator/map';

/*
  Generated class for the AddressDetailsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AddressDetailsProvider {
  address_details: any;
  constructor(public http: Http, public user: User, public api: Api) {

  }
  
  selAddressDetails(){
    var account: {user_id: string } = {
       user_id: this.user.getUser().user_id
    };  
    return this.address_details=this.api.post('address_details/SelAddressDetails.php', account).map(res => res.json());
  }
  
  loadAddressDetails(){
      if(this.address_details==null){
          return this.selAddressDetails();
      }else{
          return this.address_details;
      }
  }
  loadForce(){
      this.address_details==null;
      this.loadAddressDetails();
  }
  
  loadForcely(){
      return this.selAddressDetails();
  }
  load(){
//         return this.http.get(url + 'address_details/SelAddressDetailsClient.php') .map(res => res.json());
  }
  insAddress(accountInfo){
     accountInfo.user_id= this.user.getUser().user_id;
     let seq = this.api.post('address_details/InsAddressDetails.php', accountInfo).share();
     return seq;
  }
  deleteAddressDetails(accountInfo){
     let seq = this.api.post('address_details/DelAddressDetailsClient.php', accountInfo).map(res => res.json());
     return seq;
  }
  }