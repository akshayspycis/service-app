import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {ItemDetailPage} from "../../pages/item-detail/item-detail";

/*
  Generated class for the OrderProductProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class OrderProductProvider {
  order_products: any=[];
  constructor(public http: Http) {
    console.log('Hello OrderProductProvider Provider');
  }
  insertOrderProduct(order_product: any){
      this.order_products.push(order_product);
  }
  deleteOrderProduct(index_no: any){
      let items=[];
      this.order_products.forEach((value, index) => {
          if (!(index == index_no)){
              items.push(value);
          }
      });
      this.order_products= items;
  }
  erroseAllOrderProduct(){
      this.order_products= [];
  }
  
  selectOrderProduct(product_details_id: any){
      let items=[];
      for (let value of this.order_products) {
          if (value["product_details_id"] == product_details_id){
              items.push(value);
          }
      }
      return items;
  }
  selectAllOrderProduct(){
      let items=[];
      for (let value of this.order_products) {
              items.push(value);
      }
      return items;
  }
  updateOrderProduct(){
      
  }
}
