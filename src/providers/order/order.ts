import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {User} from "../../providers/providers";
import { Api } from '../api/api';
import 'rxjs/add/operator/map';
/*
  Generated class for the OrderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class OrderProvider {

  order: any;
  order_product: any;
  constructor(public http: Http, public user: User, public api: Api) {

  }
  
  selOrder(){
    var account: {user_id: string } = {
       user_id: this.user.getUser().user_id
    };  
    return this.order=this.api.post('order_details/SelOrderClient.php', account).map(res => res.json());
  }
  loadOrderProduct(account){
    return this.order_product=this.api.post('order_product/SelOrderProductClient.php', account).map(res => res.json());
  }
  loadPaymentDetails(account){
    return this.order_product=this.api.post('payment_details/SelPaymentDetailsClient.php', account).map(res => res.json());
  }
  loadRepairDetails(account){
    return this.order_product=this.api.post('repaire_details/SelRepaireDetailsClient.php', account).map(res => res.json());
  }
  updOrderLabExtraChargeStatus(account){
    return this.api.post('order_lab_extra_charge/UpdOrderLabExtraChargeStatusClient.php', account).map(res => res.json());
  }
  updOrderLabSpareStatus(account){
    return this.api.post('order_lab_spare/UpdOrderLabSpareStatusClient.php', account).map(res => res.json());
  }
  
  
  loadOrder(){
//      if(this.order==null){
          return this.selOrder();
//      }else{
//          return this.order;
//      }
  }
  loadForce(){
      this.order==null;
      this.loadOrder();
  }
  
  loadForcely(){
      return this.selOrder();
  }
  load(){
//         return this.http.get(url + 'order/SelOrderClient.php') .map(res => res.json());
  }
  insOrder(accountInfo){
     accountInfo.user_id= this.user.getUser().user_id;
     let seq = this.api.post('order_details/InsOrderClient.php', accountInfo).share();
     return seq;
  }
  }