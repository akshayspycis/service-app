import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { Api } from '../api/api';

/*
  Generated class for the PaytmPaymentGatewayProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PaytmPaymentGatewayProvider {

  constructor(public http: Http, public api: Api,private transfer: Transfer) {
  }
  getCheckSum(accountInfo: any) {
//    let seq = this.api.post('', accountInfo).share();
    return this.http.post('http://192.168.2.10:8081/ostp/server/payment_gateway/paytm/Paytm_App_Checksum_Kit_PHP-master/generateChecksum.php', accountInfo);
    
  }

  
  

}
