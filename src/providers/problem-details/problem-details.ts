import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {url} from '../../pages/pages';
import {ItemDetailPage} from "../../pages/item-detail/item-detail";
/*
  Generated class for the ProblemDetailsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProblemDetailsProvider {
problem_details_list: any={};    
problem_details: any;
brand_details: any={};
model_details: any={};
that:ItemDetailPage;
  constructor(public http: Http) {
    console.log('Hello ProblemDetailsProvider Provider');
  }
  loadProblemDetails(product_details_id: string,that:ItemDetailPage){
      this.that=that;
      if (this.problem_details_list[product_details_id] == undefined){
         this.selProblemDetails(product_details_id);
      }else{
          this.selProblemDetailsOnCache(product_details_id);
      }
  }
  selProblemDetails(product_details_id: string){
        let body = new FormData();
        body.append('product_details_id', product_details_id);
        return this.http.post(url + 'problem_details/SelProblemDetailsClient.php', body) .map(res => res.json()).subscribe((data)=>{
            this.problem_details_list[product_details_id]=data;
            this.selProblemDetailsOnCache(product_details_id);
        });
  }
  selProblemDetailsOnCache(product_details_id: string){
      this.that.closeLoading();
     this.problem_details=this.problem_details_list[product_details_id];
     this.brand_details={}
     this.model_details={}
     this.loadBrand();
  }
  loadBrand(){
      for (let item of this.problem_details) {
            if(item.brand_details_id!=null && item.brand_details_id!="" && this.brand_details[item.brand_details_id]==null){
               this.brand_details[item.brand_details_id]={}; 
               this.brand_details[item.brand_details_id]=item.brand_name; 
            }
            if(item.model_details_id!=null&& item.model_details_id!="" &&this.model_details[item.model_details_id]==null){
                this.model_details[item.model_details_id]={}
                this.model_details[item.model_details_id]["brand_details_id"]=item.brand_details_id;
                this.model_details[item.model_details_id]["product_details_id"]=item.product_details_id;
                this.model_details[item.model_details_id]["model_name"]=item.model_name;
            }
        }
        this.addBrandDetails();
  }
  addBrandDetails(){
      let items=[];
      for (let key of Object.keys(this.brand_details)) {
          let item = {}
          item ["brand_details_id"] = key;
          item ["brand_name"]= this.brand_details[key];
          items.push(item);
      }
      this.that.addBrandDetails(items);
  }
  
  loadModelDetails(brand_details_id: string,that:ItemDetailPage){
      this.that=that;
      let items=[];
      for (let key of Object.keys(this.model_details)) {
          if (this.model_details[key]["brand_details_id"] == brand_details_id){
              let item = {}
              item ["model_details_id"] = key;
              item ["model_name"]= this.model_details[key]["model_name"];
              items.push(item);
          }
          
      }
      this.that.addModelDetails(items);
  }
  
  loadProblem(model_details_id: string,that:ItemDetailPage){
      this.that=that;
      let items=[];
      for (let key of this.problem_details) {
          if (key["model_details_id"] == model_details_id){
              let item = {}
              item ["problem_details_id"] = key["problem_details_id"];
              item ["problem"]= key["problem"];
              item ["discription"]= key["discription"];
              items.push(item);
          }
      }
      this.that.addProblemDetails(items);
  }
  }