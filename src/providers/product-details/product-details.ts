import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {url} from '../../pages/pages';

/*
  Generated class for the ProductDetailsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProductDetailsProvider {
  product_details: any;
  constructor(public http: Http) {
    console.log('Hello ProductDetailsProvider Provider');
  }
  
  selProductDetails(){
    return this.product_details=this.http.get(url + 'product_details/SelProductDetailsClient.php') .map(res => res.json());
  }
  loadProductDetails(){
      if(this.product_details==null){
          return this.selProductDetails();
      }else{
          return this.product_details;
      }
  }
  loadForcely(){
      return this.selProductDetails();
  }
  
 load(){
     return this.http.get(url + 'product_details/SelProductDetailsClient.php') .map(res => res.json());
 }
  }