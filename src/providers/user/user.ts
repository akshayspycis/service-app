import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { Api } from '../api/api';
import {CheckotpPage} from "../../pages/checkotp/checkotp";


/**
 * Most apps have the concept of a User. This is a simple provider
 * with stubs for login/signup/etc.
 *
 * This User provider makes calls to our API at the `login` and `signup` endpoints.
 *
 * By default, it expects `login` and `signup` to return a JSON object of the shape:
 *
 * ```json
 * {
 *   status: 'success',
 *   user: {
 *     // User fields your app needs, like "id", "name", "email", etc.
 *   }
 * }
 * ```
 *
 * If the `status` field is not `success`, then an error is detected and returned.
 */
@Injectable()
export class User {
  _user: any;

  constructor(public http: Http, public api: Api,private transfer: Transfer) {
  }

  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  login(accountInfo: any) {
    let seq = this.api.post('user_details/UserLoginClient', accountInfo).share();
    return seq;
  }
  getOtp(accountInfo: any) {
    let seq = this.api.post('user_details/GenrateOtp.php', accountInfo).share();
    return seq;
  }
  updAcoountDetails(accountInfo: any) {
    let seq = this.api.post('user_details/UpdUserDetails.php', accountInfo).share();
    return seq;
  }
  
  updGenrateOtp(accountInfo: any) {
    let seq = this.api.post('user_details/UpdGenrateOtp.php', accountInfo).share();
    return seq;
  }

  /**
   * Send a POST request to our signup endpoint with the data
   * the user entered on the form.
   */
  signup(accountInfo: any) {
    let seq = this.api.post('signup', accountInfo).share();

    seq
      .map(res => res.json())
      .subscribe(res => {
        // If the API returned a successful response, mark the user as logged in
          
        if (res.status == 'success') {  
          this._loggedIn(res);
        }
      }, err => {
        console.error('ERROR', err);
      });

    return seq;
  }

  /**
   * Log the user out, which forgets the session
   */
  logout() {
    this._user = null;
    localStorage.removeItem('user_details');
  }

  /**
   * Process a login/signup response to store user data
   */
  _loggedIn(resp) {
    this._user = resp;
    localStorage.setItem('user_details', JSON.stringify(resp));
  }
  checkOtp(accountInfo){
    let seq = this.api.post('user_details/CheckOtp.php', accountInfo).share();
    return seq;
  }
  checkEmail(accountInfo){
    let seq = this.api.post('user_details/CheckEmail.php', accountInfo).share();
    return seq;
  }
  insUserDetails(accountInfo){
    let seq = this.api.post('user_details/InsUserDetailsClient.php', accountInfo).share();
    return seq;
  }
  updUserPassword(accountInfo){
    let seq = this.api.post('user_details/UpdUserPassword.php', accountInfo).share();
    return seq;
  }
  insUserPic(accountInfo){
      let seq = this.api.post('user_details/InsUserProfileDetails.php', accountInfo).share();
      return seq;
  }
  getUser(){
      if(this._user==null){
          if(localStorage.getItem('user_details')!=null){
              this._user = JSON.parse(localStorage.getItem('user_details'));
          }
      }
      return this._user;
  }
  setAccounDetails(account){
      this._user.user_name = account.user_name;
      this._user.email= account.email;
      this._user.contact_no= account.contact_no;
  }
  
}
